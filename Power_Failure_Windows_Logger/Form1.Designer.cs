﻿namespace Power_Failure_Windows_Logger
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.timer_poll = new System.Windows.Forms.Timer(this.components);
            this.lblStatus = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTtitle = new System.Windows.Forms.Label();
            this.pbStatus = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblLastFailureDate = new System.Windows.Forms.Label();
            this.lblLastFailureTime = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbStatus)).BeginInit();
            this.SuspendLayout();
            // 
            // timer_poll
            // 
            this.timer_poll.Enabled = true;
            this.timer_poll.Interval = 1000;
            this.timer_poll.Tick += new System.EventHandler(this.timer_poll_Tick);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(164, 39);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(58, 17);
            this.lblStatus.TabIndex = 0;
            this.lblStatus.Text = "novalue";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Current Power Status:";
            // 
            // lblTtitle
            // 
            this.lblTtitle.AutoSize = true;
            this.lblTtitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTtitle.Location = new System.Drawing.Point(12, 9);
            this.lblTtitle.Name = "lblTtitle";
            this.lblTtitle.Size = new System.Drawing.Size(149, 18);
            this.lblTtitle.TabIndex = 2;
            this.lblTtitle.Text = "Power Failure Logger";
            // 
            // pbStatus
            // 
            this.pbStatus.BackColor = System.Drawing.Color.Black;
            this.pbStatus.Location = new System.Drawing.Point(228, 39);
            this.pbStatus.Name = "pbStatus";
            this.pbStatus.Size = new System.Drawing.Size(20, 20);
            this.pbStatus.TabIndex = 3;
            this.pbStatus.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(163, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Last Power Failure Date:";
            // 
            // lblLastFailureDate
            // 
            this.lblLastFailureDate.AutoSize = true;
            this.lblLastFailureDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastFailureDate.Location = new System.Drawing.Point(181, 69);
            this.lblLastFailureDate.Name = "lblLastFailureDate";
            this.lblLastFailureDate.Size = new System.Drawing.Size(80, 17);
            this.lblLastFailureDate.TabIndex = 5;
            this.lblLastFailureDate.Text = "00/00/0000";
            // 
            // lblLastFailureTime
            // 
            this.lblLastFailureTime.AutoSize = true;
            this.lblLastFailureTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastFailureTime.Location = new System.Drawing.Point(182, 95);
            this.lblLastFailureTime.Name = "lblLastFailureTime";
            this.lblLastFailureTime.Size = new System.Drawing.Size(44, 17);
            this.lblLastFailureTime.TabIndex = 6;
            this.lblLastFailureTime.Text = "00:00";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Last Power Failure Time:";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 155);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblLastFailureTime);
            this.Controls.Add(this.lblLastFailureDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pbStatus);
            this.Controls.Add(this.lblTtitle);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblStatus);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "Power Failure Logger Program";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbStatus)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer_poll;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTtitle;
        private System.Windows.Forms.PictureBox pbStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblLastFailureDate;
        private System.Windows.Forms.Label lblLastFailureTime;
        private System.Windows.Forms.Label label3;
    }
}

