﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Reflection; //needed to gather system power inforamtion
using System.IO; //needed to write to a txt file for logging purposes. 

/*
 * Programmed by: Peter Stevenson (2E0PGS)
 * Licence: GNU General Public Licence
 * This program is designed to log the power falures of a laptop to txt file
 * This program uses Microsoft .NET Framework 4. This enabled the program to work on XP. 
 * Tested OS (Windows 7, Windows XP). Howver should work on Windows XP or newer. 
*/

namespace Power_Failure_Windows_Logger
{
    public partial class frmMain : Form
    {
        string strPowerStatus; //variable that stores String
        int intPowerChanged; //variable that keeps track of if we changed power state.

        public frmMain()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }        

        private void timer_poll_Tick(object sender, EventArgs e) //poll system for current power state
        {
            Boolean isRunningOnBattery = (System.Windows.Forms.SystemInformation.PowerStatus.PowerLineStatus == PowerLineStatus.Offline);
            if (isRunningOnBattery == true)
            {
                if (intPowerChanged == 1) //if we changed power state run this once
                {
                    strPowerStatus = "Offline";
                    pbStatus.BackColor = Color.Red;
                    lblLastFailureTime.Text = DateTime.Now.ToString("HH:mm ss tt"); //update "last power failure" labels on form1
                    lblLastFailureDate.Text = DateTime.Now.ToString("dddd dd/MM/yyyy");
                    //------------------Write-To-Log-File------------------// Power Failed
                    StreamWriter writer = new StreamWriter("PowerFailureLog.txt", true);
                    writer.Write("Power Failed: ");
                    writer.Write(DateTime.Now.ToString("HH:mm ss tt "));
                    writer.WriteLine(DateTime.Now.ToString("dddd dd/MM/yyyy"));
                    writer.Close();
                    //---------END------Write-To-Log-File---------END------//
                    lblStatus.Text = strPowerStatus; //update status label
                    intPowerChanged = 0;
                }
            }
            else
            {
                if (intPowerChanged == 0) //if we changed power state run this once
                {
                    strPowerStatus = "Online";
                    pbStatus.BackColor = Color.LightGreen;
                    //------------------Write-To-Log-File------------------// Power Restored
                    StreamWriter writer = new StreamWriter("PowerFailureLog.txt", true);
                    writer.Write("Power Restored: ");
                    writer.Write(DateTime.Now.ToString("HH:mm ss tt "));
                    writer.WriteLine(DateTime.Now.ToString("dddd dd/MM/yyyy"));
                    writer.Close();
                    //---------END------Write-To-Log-File---------END------//
                    lblStatus.Text = strPowerStatus; //update status label
                    intPowerChanged = 1;
                }
            }            
        }

        
       
    }
}
