########################################################################
THIS IS A C# BASED PROGRAM FOR LOGGING POWER FAILURES ON A LAPTOP.

Features:
*Logs power failures to text file. Including time and date. 
*Logs when the power is restored to text file. Including time and date. 
*GUI
 *Displays time and date of last power failures
 *Displays current power status. (Online, Offline)
 *Displays a Green or Red status icon. 

Licence:
*This program is licensed under GNU General Public Licence
*http://opensource.org/licenses/GPL-3.0

Operating System Support:
*Tested on Windows 7 and Windows XP
*Should work on Windows XP or Newer
*Microsoft .net Framework version 4.

How do I download the .exe ?
*You need to find the download section. 
*Navigate to download section on the repository. 
*The Build depends on if I have compiled it recently. Which I do very often.
*Once you got the .exe file it will run from it like a portable program. NO INSTALL REQUIRED.
*However you do need Microsoft .net Framework 4. 

########################################################################